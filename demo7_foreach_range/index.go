package main

import "fmt"

func main() {
	courses := []string{"JAVA", "Kotlin", "React"}
	for intdex, item := range courses {
		fmt.Printf("%d, %s \n", intdex+1, item)
	}

	for _, item := range courses {
		fmt.Printf("%s \n", item)
	}
}
