package main

import "fmt"

func main() {
	fn1()
	fn2("ammy")
	fn3("ammy", 1)

	const a, b = 5, 7
	fmt.Printf("%d+%d=%d", a, b, sum(a, b))

	x, y := swap(a, b)
	fmt.Printf("\n%d,%d=>%d,%d\n", a, b, x, y)

	_x, _y, title := swapV2(10, 20)
	fmt.Printf("%d,%d=>%d,%d,%s\n", 10, 20, _x, _y, title)
}

func fn1() {
	fmt.Println("test go :fn1")
}

func fn2(msg string) {
	fmt.Println(msg)
}

func fn3(msg string, version int) {
	fmt.Print(msg)
	fmt.Println(version)
}

func sum(a int, b int) int {
	return a + b
}

func swap(a int, b int) (int, int) {
	return b, a
}

func swapV2(a int, b int) (x, y int, title string) {
	y = a
	x = b
	title = "Result"
	return
}
