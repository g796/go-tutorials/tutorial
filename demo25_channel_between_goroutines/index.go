package main

import (
	"fmt"
	"time"
)

func routin1(c chan int, payload int) {
	c <- payload
	fmt.Println("step 1")
	c <- payload
	fmt.Println("step 2")
	c <- payload
	fmt.Println("step 3")
}

func main() {
	ch := make(chan int, 3)
	go routin1(ch, 1)

	fmt.Println(<-ch)
	fmt.Println(<-ch)
	fmt.Println(<-ch)
	time.Sleep(1 * time.Second)

}
