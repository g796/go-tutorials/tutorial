package main

import (
	"fmt"
)

func routin1(c chan int, countTo int) {
	for i := 0; i < countTo; i++ {
		c <- i
	}
	close(c)
}

func main() {
	ch := make(chan int, 1)
	go routin1(ch, 10)

	for true {
		value, ok := <-ch
		if !ok {
			fmt.Println("No mor data")
			break
		}
		fmt.Println(value)
	}

	/*
		for v := range ch {
			fmt.Println(v)
		}
		fmt.Println("No mor data")
	*/
}
