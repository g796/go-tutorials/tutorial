package main

import "fmt"

func main() {
	msg := "hello eiei"
	var msgPointer *string = &msg
	fmt.Println(msg)
	fmt.Println(msgPointer)
	fmt.Println(*msgPointer)

	changMessage(&msg, "new message 1")
	fmt.Println(msg)

	changMessage(msgPointer, "new message 2")
	fmt.Println(msg)

	changMessage(msgPointer, "new message 3")
	fmt.Println(*msgPointer)
}

func changMessage(aPointer *string, newMessage string) {
	*aPointer = newMessage
}
