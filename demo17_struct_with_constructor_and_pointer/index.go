package main

import "demo15/employee"

func main() {
	e := employee.Init("vipawadee", "A.", 30, 20)
	e.LeavesRemaining()

	e = employee.Init("sarawut", "A.", 30, 20)
	e.LeavesRemaining()

}
