package employee

import (
	"fmt"
)

type employee struct {
	FirstName   string
	LastName    string
	TotalLeaves int
	LeavesTaken int
}

// * is pointer
var employeeInstance *employee //for ckeck instance

func Init(firstname string, lastname string, totalleaves int, leavestaken int) *employee {
	if employeeInstance == nil { //ckeck instance
		employeeInstance = &employee{
			FirstName:   firstname,
			LastName:    lastname,
			TotalLeaves: totalleaves,
			LeavesTaken: leavestaken,
		}
	}
	return employeeInstance
}

func (e employee) LeavesRemaining() {
	fmt.Printf("%s %s has %d leaves remaining\n", e.FirstName, e.LastName, (e.TotalLeaves - e.LeavesTaken))
}
