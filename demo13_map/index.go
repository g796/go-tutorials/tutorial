package main

import "fmt"

func main() {
	var numbers = map[string]int{"one": 1, "two": 2}
	fmt.Println(numbers)
	fmt.Println(numbers["one"])

	var colors = make(map[string]string)
	colors["red"] = "แดง"
	colors["green"] = "เขียว"
	colors["blue"] = "น้ำเงิน"
	fmt.Println(colors)
	fmt.Println(colors["red"])

	var courses = make(map[string]map[string]int)
	courses["JAVA"] = make(map[string]int)
	courses["JAVA"]["price"] = 300
	courses["JAVA"]["price"] = 100
	courses["JAVA"]["code"] = 200
	courses["GO"] = make(map[string]int)
	courses["GO"]["price"] = 400
	courses["GO"]["code"] = 200
	fmt.Println(courses)
}
