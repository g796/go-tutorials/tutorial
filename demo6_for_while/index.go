package main

import "fmt"

func main() {
	fnFor()
	fnWhile()
	fnWhileUsingBreak()

}

func fnFor() {
	for index := 0; index <= 10; index++ {
		fmt.Printf("index %d \n", index)
	}
}

func fnWhile() {
	index := 0
	for index < 5 {
		index++
		fmt.Printf("i %d \n", index)

	}
}

func fnWhileUsingBreak() {
	index := 0
	for true {
		if index > 5 {
			break
		}
		fmt.Printf("%d \n", index)
		index++
	}
}
