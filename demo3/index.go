package main

import "fmt"

func main() {
	fmt.Println("Begin")

	// Explicit Declaration
	var tmp1 int = 0
	tmp1 = 10
	var tmp2 string = "hello"
	var tmp3 bool = true

	// constant
	const tmp4 int = 0

	tmp5 := 0
	tmp6 := "ammy"

	fmt.Println(tmp1)
	fmt.Println(tmp2)
	fmt.Println(tmp3)
	fmt.Println(tmp4)
	fmt.Println(tmp5)
	fmt.Println(tmp6)

}
